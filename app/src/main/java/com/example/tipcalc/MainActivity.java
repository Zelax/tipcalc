package com.example.tipcalc;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.texttotal)
    EditText texttotal;
    @BindView(R.id.textpropina)
    EditText textpropina;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.botoncalcular, R.id.botonmas, R.id.botonmenos})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.botoncalcular:
                break;
            case R.id.botonmas:
                break;
            case R.id.botonmenos:
                break;
        }
    }
}
